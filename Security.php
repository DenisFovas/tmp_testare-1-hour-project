<?php

require_once 'UserValidator.php';
require_once 'UserRepository.php';

class Security
{
    /**
     * @var UserValidator
     */
    private $userValidator;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Security constructor.
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->userValidator = new UserValidator();
    }

    public function registerUser()
    {
        $userData = [
            'username' => $_POST['user["username"]'] ?? null,
            'email' => $_POST['user["email"]'] ?? null,
            'password' => $_POST['user["password"]'] ?? null,
        ];

        if (in_array(null, $userData)) {
            http_redirect('/index.php', null, null, 304);
            return;
        }

        $user = new User(null, $userData['email'], $userData['username'], md5($userData['password']));

        if ($this->userValidator->validate($user)) {
            $this->userRepository->create($user);
            http_redirect('/login.php', null, null, 201);

            return;
        } else {
            http_redirect('/index.php', null, null, 304);
        }


    }
}