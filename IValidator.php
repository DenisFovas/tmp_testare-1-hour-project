<?php


interface IValidator
{
    public function validate($entity);
}