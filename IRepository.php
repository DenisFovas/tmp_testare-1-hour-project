<?php


interface IRepository
{
    public function findOne(int $id);

    public function findAll();

    public function deleteById(int $id);

    public function update(int $id, $object);

    public function create($object);
}