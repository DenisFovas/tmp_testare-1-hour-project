<?php

require_once 'IValidator.php';
require_once 'User.php';
require_once 'UserRepository.php';

class UserValidator implements IValidator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserValidator constructor.
     */
    public function __construct()
    {
        $userRepository = new UserRepository();
    }


    public function validate($entity)
    {
        if (!$entity instanceof User) {
            return false;
        }

        if (strlen($entity->getPassword()) < 6) {
            return false;
        }

        if (!preg_match('/[A-Z]/', $entity->getPassword())) {
            return false;
        }

        if (!preg_match('/[A-Za-z0-9.+_\-&%!?]@[A-Za-z0-9.+_\-]\.[a-z]/', $entity->getEmail())) {
            return false;
        }

        /** @var User[] $users */
        $users = $this->userRepository->findAll();

        foreach ($users as $user) {
            if ($user->getEmail() === $entity->getEmail()) {
                return false;
            }
            if ($user->getUsername()=== $entity->getUsername()) {
                return false;
            }
        }
    }
}