<?php

require_once 'UserRepository.php';

class UI
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * UI constructor.
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }


    public function register()
    {
        echo '<form action="/register.php" method="POST">
                <label>Username: </label> <input type="text" name="user[username[">
                <label>Email: </label> <input type="email" name="user[email]">
                <label>Password: </label> <input type="password" name="user[password]">
                <button type="submit">Register</button>
            </form>';
    }

    public function listing()
    {
        echo '<ul>';
        /** @var User[] $users */
        $users = $this->userRepository->findAll();

        foreach ($users as $user) {
            echo '<li>';
            echo sprintf('Username: %s - Email %s', $user->getUsername(), $user->getEmail());
            echo '</li>';
        }

        echo "</ul>";
    }

    public function failureToRegister()
    {
        echo '<h2> Can\'t connect to the server </h2>';
    }
}