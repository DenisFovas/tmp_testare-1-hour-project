--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(255),
    password character varying(255),
    email character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, password, email) FROM stdin;
1	demo	demo	demo
3	 ds	123	a
5	demo1	demo1	demo1
6	demo2	demo2	demo2
8	demo2dwas	demo2@GMAIL.com	demo2dsada
15	denis2	denisfovas@GMAIL.com	4297F44B13955235245B2497399D7A93
25	denis23	4297F44B13955235245B2497399D7A93	denisfovas@GMAIL.com
26	123denisaddasfdas	4297F44B13955235245B2497399D7A93	fdsaj@GMAIL.com
34	denis233	4297F44B13955235245B2497399D7A93	denisfovas12@GMAIL.com
35	123deni1saddasfdas	4297F44B13955235245B2497399D7A93	fdsaj23@GMAIL.com
36	jfadshlj1kashfdlsa	4297F44B13955235245B2497399D7A93	dfnsk123@23aGMAIL.com
37	denis2331	4297F44B13955235245B2497399D7A93	asda@GMAIL.com
38	1123deni1saddasfdas	4297F44B13955235245B2497399D7A93	dfsjahfdsaj23@GMAIL.com
39	fdshafasjklfh	4297F44B13955235245B2497399D7A93	dfnsk12dsfs3@GMAIL.com
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 39, true);


--
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk UNIQUE (id);


--
-- Name: users_email_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_email_uindex ON public.users USING btree (email);


--
-- Name: users_username_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_username_uindex ON public.users USING btree (username);


--
-- PostgreSQL database dump complete
--

