<?php

require_once 'IRepository.php';
require_once 'User.php';

class UserRepository implements IRepository
{
    private const INSERT_QUERY = "INSERT INTO users (id, username, email, password) VALUES (nextval('users_id_seq'), $1, $2, $3)";

    private const FIND_ALL_QUERY = 'SELECT * FROM users;';

    private $connection;
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->connection =  pg_connect("host=localhost dbname=tmp_testare_crud user=postgres password=postgres")
            or die('Could not connect: ' . pg_last_error());
    }

    public function findOne(int $id)
    {
        // TODO: Implement findOne() method.
    }

    public function findAll()
    {
        $statement = pg_prepare($this->connection, 'find_all_query', self::FIND_ALL_QUERY);

        // TODO: Handle the error of statement prepare cause is the same for all.
        if (!$statement) {
            die('Error when preparing query');
        }

        $result = pg_execute($this->connection, 'find_all_query', []);

        $users = [];
        foreach ($result as $userData) {
            $user = new User($userData['id'], $userData['email'], $userData['username'], null);

            $users[] = $user;
        }

        return $users;
    }

    public function deleteById(int $id)
    {
        // TODO: Implement deleteById() method.
    }

    public function update(int $id, $object)
    {
        // TODO: Implement update() method.
    }

    public function create($object)
    {
        if (!$object instanceof User) {
            return;
        }

        $statement = pg_prepare($this->connection, 'create_query', self::INSERT_QUERY);

        if (!$statement) {
            die('Error when preparing query');
        }

        $result = pg_execute($this->connection, 'create_query', [$object->getUsername(), $object->getEmail(), $object->getPassword()]);
    }
}